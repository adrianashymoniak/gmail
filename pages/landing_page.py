from selenium.webdriver.common.by import By

from data.email import Email
from pages.base_page import BasePage
from pages.new_message_page import NewMessagePage


class LandingPage(BasePage):
    def __init__(self, driver):
        self.driver = driver

    def title(self):
        # change locator it not stable
        return BasePage.get_attribute(self, (By.XPATH, "//a[@class='gb_b gb_cb gb_R']"), "title")

    def click_compose_button(self):
        BasePage.click(self, (By.XPATH, "//div[@class='T-I J-J5-Ji T-I-KE L3']"))
        return NewMessagePage(self.driver)

    def send_email(self, email):
        new_message_page = self.click_compose_button()

        if email.email is not None:
            new_message_page.type_email(email.email)
        if email.subject is not None:
            new_message_page.type_subject(email.subject)
        if email.message is not None:
            new_message_page.type_message(email.message)

        new_message_page.click_send_button()

    def find_text(self):
        return BasePage.get_text(self, (By.XPATH, "//div[@class='y6']"))
