from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from pages.login_page import LoginPage


class HomePage(BasePage):
    def __init__(self, driver):
        self.driver = driver

    def load(self):
        self.driver.get("https://www.google.com.ua/")

    def click_sign_in(self):
        BasePage.click(self, (By.CLASS_NAME, "gb_P"))
        return LoginPage(self.driver)
