from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class BasePage():
    def __init__(self, driver):
        self.driver = driver

    def log_exception(self, exception):
        print("An exception of type %s occurred." % type(exception).__name__)

    def log_info(self, info):
        print("%s\n" % info)

    def get_visible(self, locator):
        try:
            element = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(locator))
            self.log_info("Element visible by locator <%s>" % str(locator))
            return element
        except Exception as e:
            self.log_exception(e)
            self.log_info("Element not visible by locator <%s>" % str(locator))
            raise

    def click(self, locator):
        try:
            self.get_visible(locator).click()
            self.log_info("Clicked element found by locator <%s>" % str(locator))
        except Exception as e:
            self.log_exception(e)
            self.log_info("Failed to click on element by locator <%s>" % str(locator))
            raise

    def send_keys(self, locator, text):
        try:
            value = self.get_visible(locator)
            self.get_visible(locator).send_keys(text)
            self.log_info("Typed <%s> in <%s>" % (text, str(locator)))
            return value
        except Exception as e:
            self.log_exception(e)
            self.log_info("Value <%s> not typed in element by locator <%s>" % (text, str(locator)))
            raise

    def get_attribute(self, locator, attribute):
        try:
            value = self.get_visible(locator).get_attribute(attribute)
            self.log_info("Attribute <%s> equals to <%s> in <%s>" % (attribute, value, str(locator)))
            return value
        except Exception as e:
            self.log_exception(e)
            self.log_info("Attribute <%s> not found element by locator <%s>" % (attribute, str(locator)))
            raise

    def get_text(self, locator):
        try:
            value = self.get_visible(locator).text
            self.log_info("Text is equal to <%s> found in locator <%s>" % (value, locator))
            return value
        except Exception as e:
            self.log_exception(e)
            self.log_info("Text not found by locator <%s>" % locator)
            raise

