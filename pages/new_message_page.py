from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class NewMessagePage(BasePage):
    def __init__(self, driver):
        self.driver = driver

    def type_email(self, email):
        BasePage.send_keys(self, (By.NAME, "to"), email)

    def type_subject(self, subject):
        BasePage.send_keys(self, (By.NAME, "subjectbox"), subject)

    def type_message(self, message):
        BasePage.send_keys(self, (By.XPATH, "//div[@aria-label='Message Body']"), message)

    def click_send_button(self):
        BasePage.click(self, (By.XPATH, "//div[@data-tooltip = 'Send ‪(Ctrl-Enter)‬']"))

