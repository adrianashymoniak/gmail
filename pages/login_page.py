from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from pages.landing_page import LandingPage


class LoginPage(BasePage):
    def __init__(self, driver):
        self.driver = driver

    def type_email(self, email):
        BasePage.send_keys(self, (By.NAME, "identifier"), email)

    def type_password(self, password):
        BasePage.send_keys(self, (By.NAME, "password"), password)

    def click_next_button(self):
        BasePage.click(self, (By.XPATH, "//span[@class = 'RveJvd snByac']"))

    def login(self, user):
        self.type_email(user.email)
        self.click_next_button()
        self.type_password(user.password)
        self.click_next_button()
        return LandingPage(self.driver)
