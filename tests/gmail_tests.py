from ddt import ddt, data

from data.user import User
from data.user_ddt import UserDDT
from pages.home_page import HomePage
from tests.base_test import BaseTest


@ddt
class GmailTest(BaseTest):
    @data(UserDDT(User("adrianashymoniak@gmail.com", "MoLoD:3/80"), "Adriana Shymoniak"),
          UserDDT(User("adrianashymoniak@gmail.com", "MoLoD:3/80"), "Adriana Shymoniak"))
    def test_login(self, user_ddt):
        homepage = HomePage(self.driver)
        homepage.load()
        login_page = homepage.click_sign_in()

        landing_page = login_page.login(user_ddt.user)
        self.assertIn(user_ddt.full_name, landing_page.title(), "Full name should be present in title")
