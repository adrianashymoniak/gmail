from ddt import ddt, data
from datetime import datetime
from data.email import Email
from data.user import User
from data.user_ddt import UserDDT
from pages.home_page import HomePage
from pages.new_message_page import NewMessagePage
from tests.base_test import BaseTest


@ddt
class SendMessage(BaseTest):
    @data(UserDDT(User("adrianashymoniak@gmail.com", "MoLoD:3/80"), "Adriana Shymoniak"))
    def test_send_message(self, user_ddt):
        homepage = HomePage(self.driver)
        homepage.load()
        login_page = homepage.click_sign_in()
        landing_page = login_page.login(user_ddt.user)
        current_time = datetime.now().time()
        email = Email("adrianashymoniak@gmail.com", ("Test" + str(current_time)))
        landing_page.send_email(email)

        self.assertIn(("Test" + str(current_time)), landing_page.find_text(), "Text not found")
